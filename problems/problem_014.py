# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if "flour" in ingredients and "eggs" in ingredients and "oil" in ingredients:
        return print("can make pasta")
    else:
        return print("can't make pasta")
can_make_pasta(["flour", "oil", "eggs"])